
CREATE TABLE `CreditCards` (
  `CreditCardId` int(11) NOT NULL AUTO_INCREMENT,
  `CreditCardType` varchar(16) DEFAULT NULL,
  `CreditCardName` varchar(50) DEFAULT NULL,
  `CreditCardNumber` varchar(16) DEFAULT NULL,
  `ExpiryDate` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`CreditCardId`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;


CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `streetnumber` varchar(45) DEFAULT NULL,
  `streetname` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `province` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `postalcode` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `hotel` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `mcda551002`.`hotel`(`id`,`name`,`address`)VALUES('1', 'Sheraton', 'Geneva');
INSERT INTO `mcda551002`.`hotel`(`id`,`name`,`address`)VALUES('2', 'Royal', 'Harvester');
INSERT INTO `mcda551002`.`hotel`(`id`,`name`,`address`)VALUES('3', 'Redmont', 'PHOENIX AZ 85123');
INSERT INTO `mcda551002`.`hotel`(`id`,`name`,`address`)VALUES('4', 'Fairmont', 'Halifax');

CREATE TABLE `Reservation` (
  `ReservationId` int(11) NOT NULL,
  `CheckInDate` varchar(45) DEFAULT NULL,
  `CheckOutDate` varchar(45) DEFAULT NULL,
  `RoomNumber` varchar(10) DEFAULT NULL,
  `ReservationStatus` varchar(15) DEFAULT NULL,
  `CustomerId` int(11) DEFAULT NULL,
  `CreditCardId` int(11) DEFAULT NULL,
  `HotelId` int(11) DEFAULT NULL,
  `AgentId` varchar(100) DEFAULT NULL,
  `Guests` int(11) DEFAULT NULL,
  PRIMARY KEY (`ReservationId`),
  KEY `FK_CUST_ID_idx` (`CustomerId`),
  KEY `FK_CREDIT_ID_idx` (`CreditCardId`),
  KEY `FK_HOTEL_ID_idx` (`HotelId`),
  CONSTRAINT `FK_CREDIT_ID` FOREIGN KEY (`CreditCardId`) REFERENCES `CreditCards` (`CreditCardId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_CUST_ID` FOREIGN KEY (`CustomerId`) REFERENCES `customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_HOTEL_ID` FOREIGN KEY (`HotelId`) REFERENCES `hotel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


