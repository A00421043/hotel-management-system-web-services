


#README For Hotel Management Project#

#######DATE 29-NOV-2018#####

TASKS TO BE COMPLETED FOR THE NEXT SPRINT

>> UPDATE RESERVATION CLASS TO BE DELIVERED BY 30-NOV-2017. ~ JANTI,NITIN,MAROOF

>> UPDATE Views to Accept the JSON and show in MVC - MAROOF

>> GET CUSTOMER JSON AND MAKE CONTROLLERS TO PRESENT IT USING ASP.NET ~ NITIN

>> MAKE CUSTOMER VIEWS ~ ALYA

>> UPLOAD .WAR FILE AND DEPLOY THE WEB SERVICE ON THE LINUX DEV SERVER (dev.cs.smu.ca) ~ MAROOF,FUNTO


>> MEET ON SUNDAY FOR REVIEW MEETING 3-DEC-2017.

TASKS COMPLETED
>> VALIDATION TO BE DELIVERED BY 30-NOV-2017. --FUNTO

#######DATE 27-NOV-2018#####


TASKS TO BE COMPLETED FOR THE NEXT SPRINT

>> UPDATE RESERVATION CLASS TO BE DELIVERED BY 30-NOV-2017. JANTI,NITIN,MAROOF


>> VALIDATION TO BE DELIVERED BY 30-NOV-2017. --FUNTO & ALYA


>> MEET ON SUNDAY FOR REVIEW MEETING 3-DEC-2017.



###################Progress################ DATE 26-nov-2017

Completed: 

Nitin, Maroof, and Janti

> Created Customer class, DAO, and controller
> Created Reservation class, DAO, and controller
> Created createhotelDB.SQL
> Updated JDB for all classes
> Created connection between .NET and Java (IIS and Tomcat)
> Updated .NET project methods to pass JSON object
> Updated Java classes (POST) to accept the JSON object

Funto:

> Created CreditCard class, DAO, and controller

Alya:

> Created Hotel class, DAO, and controller

Next Steps:

> Must update the Reservation method (Nitin)
> Follow up with team on Monday (Team)

###################Team Member################ DATE 20-nov-2017
Nitin
-- To Add  on MVC the following

--To modify solution to view the reservation history 

-- To Add another model Hotel.

-- To  Add functionality for cancellation and Expired reservations.

-- To Add Login Feature

Akash, Nitin ----------------->Reservation

Alya ------------------------->Hotel

Funto ------------------------>Credit Card

Janti Aye -------------------->Reservation

Maroof Pasha------------------>Customer

#############################################

Add cancelation.

Add Hotel.
##########################Functionality#############################################

Reservation

Add username

Add hotel

############################################
Cancelation

Edit

Search

##########################DATE 22-NOV-2017
STATUS UPDATE FROM LAST SPRINT

-- To Add  on MVC the following --Completed

--To modify solution to view the reservation history --Completed

-- To Add another model Hotel.--Completed

-- To  Add functionality for cancellation and Expired reservations.--Completed

-- To Add Login Feature --Completed 

Akash, Nitin ----------------->Reservation --Pending

Alya ------------------------->Hotel  --Pending

Funto ------------------------>Credit Card --Pending

Janti Aye -------------------->Reservation --Pending

Maroof Pasha------------------>Customer --Pending

To be delivered before Next sprint

Akash, Nitin ----------------->Reservation 

Alya ------------------------->Hotel  

Funto ------------------------>Credit Card 

Janti Aye -------------------->Reservation 

Maroof Pasha------------------>Customer 



