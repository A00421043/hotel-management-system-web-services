﻿SET IDENTITY_INSERT [dbo].[Reservations] ON
INSERT INTO [dbo].[Reservations] ([ReservationId], [CustomerId], [CreditCardId], [CheckInDate], [CheckOutDate], [RoomNumber], [Guests], [ReservationStatus], [AgentId]) VALUES (1, 1, 1, N'2017-11-02 00:00:00', N'2017-11-04 00:00:00', N'Room 105', 2, N'Active', NULL)
INSERT INTO [dbo].[Reservations] ([ReservationId], [CustomerId], [CreditCardId], [CheckInDate], [CheckOutDate], [RoomNumber], [Guests], [ReservationStatus], [AgentId]) VALUES (3, 4, 6, N'2017-11-22 00:00:00', N'2017-11-23 00:00:00', N'Room 101', 2, N'Active', N'2ce1fe45-4025-4d69-b462-8ed90248a63f')
INSERT INTO [dbo].[Reservations] ([ReservationId], [CustomerId], [CreditCardId], [CheckInDate], [CheckOutDate], [RoomNumber], [Guests], [ReservationStatus], [AgentId]) VALUES (4, 5, 7, N'2017-11-22 00:00:00', N'2017-11-23 00:00:00', N'Room 101', 1, N'Cancel', N'2ce1fe45-4025-4d69-b462-8ed90248a63f')
SET IDENTITY_INSERT [dbo].[Reservations] OFF
