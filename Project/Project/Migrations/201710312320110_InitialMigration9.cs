namespace Project.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration9 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CreditCards", "ExpiryDate", c => c.String(nullable: false, maxLength: 7));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CreditCards", "ExpiryDate", c => c.String(nullable: false, maxLength: 4));
        }
    }
}
