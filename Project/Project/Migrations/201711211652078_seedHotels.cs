namespace Project.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seedHotels : DbMigration
    {
        public override void Up()
        {
            Sql(@"INSERT INTO HOTELS(NAME,ADDRESS) VALUES('Sheraton','Geneva');
                  INSERT INTO HOTELS(NAME,ADDRESS) VALUES('Royal','Harvester ');
                  INSERT INTO HOTELS(NAME,ADDRESS) VALUES('Redmont','PHOENIX AZ 85123');
                  INSERT INTO HOTELS(NAME,ADDRESS) VALUES('Fairmont','Halifax');");
        }
        
        public override void Down()
        {
        }
    }
}
