namespace Project.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Reservations", "RoomNumber", c => c.String(nullable: false, maxLength: 8));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Reservations", "RoomNumber", c => c.String(nullable: false, maxLength: 3));
        }
    }
}
