﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Project.Models;

namespace Project.ViewModels
{
    public class ViewModelReservationPage
    {
        public int? Id { get; set; }
        public CreditCard CreditCard { get; set; }
        public Customer Customer { get; set; }
        public Reservation Reservation { get; set; }

        public IEnumerable<Hotel> Hotels { get; set; }
        public ViewModelReservationPage()
        {
            Id = 0;
        }
        public ViewModelReservationPage(CreditCard creditCard, Customer customer, Reservation reservation)
        {
            CreditCard = creditCard;
            Customer = customer;
            Reservation = reservation;
        }
    }
}