﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project.Models
{
    public class CreditCard
    {
        [Required]
        public int CreditCardId { get; set; }
        [Required(ErrorMessage = "Credit Card Type Required")]
        [StringLength(16)]
        public String CreditCardType { get; set; }
        [Required(ErrorMessage = "Credit Card Name Required")]
        [RegularExpression(@"^[a-zA-Z\s]+$", ErrorMessage = "Credit card name only accepts letters a-z or A-Z!")]
        [StringLength(50)]
        public String CreditCardName { get; set; }
        [Required(ErrorMessage = "Credit Card Number Required")]
        [CreditCard(ErrorMessage = "Please enter a valid credit card number!")]
        [StringLength(16)]
        public String CreditCardNumber { get; set; }
        [Required(ErrorMessage = "Expiry Date Type Required")]
        [RegularExpression(@"^((0[1-9])|(1[0-2]))[\/\.\-]*((201[7-9])|(202[0-9])|(203[0-1]))$", ErrorMessage = "Please enter a valid expiry date! Format: MM/YYYY between 2017 and 2031")]
        [StringLength(7)]
        public String ExpiryDate { get; set; }
    }
}