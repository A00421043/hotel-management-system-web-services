﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Project.Models
{
    public class Reservation
    {
        [Required]
        public int ReservationId { get; set; }
        public virtual int CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }
        public virtual int CreditCardId { get; set; }
        [ForeignKey("CreditCardId")]
        public virtual CreditCard CreditCard { get; set; }
        [Required(ErrorMessage = "Check In Date Required")]
        [DataType(DataType.Date)]
        [CheckInDateValidation]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CheckInDate { get; set; }
        [Required(ErrorMessage = "Check Out Date Required")]
        [DataType(DataType.Date)]
        [CheckOutDateValidation]
        [CheckOutGreaterCheckIn]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CheckOutDate { get; set; }
        [Required(ErrorMessage = "Room Required")]
        [StringLength(8)]
        public String RoomNumber { get; set; }
        [Required(ErrorMessage = "Number of Guests Required")]
        public int Guests { get; set; }
        public String ReservationStatus { get; set; }

        public String AgentId { get; set; }

        public Hotel Hotel { get; set; }
        public int HotelId { get; set; }
    }
}