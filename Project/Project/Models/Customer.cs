﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project.Models
{
    public class Customer
    {
        [Required]
        public int CustomerId { get; set; }
        [Required(ErrorMessage = "First Name Required")]
        [RegularExpression(@"^[a-zA-Z\s]+$", ErrorMessage = "First name only accepts letters a-z or A-Z!")]
        [StringLength(50)]
        public String FirstName { get; set; }
        [Required(ErrorMessage = "Last Name Required")]
        [RegularExpression(@"^[a-zA-Z\s]+$", ErrorMessage = "Last name only accepts letters a-z or A-Z!")]
        [StringLength(50)]
        public String LastName { get; set; }
        [Required(ErrorMessage = "Street Number Required")]
        [RegularExpression(@"^[0-9]+$",ErrorMessage = "Street number only accepts numbers!")]
        [StringLength(6)]
        public String StreetNumber { get; set; }
        [Required(ErrorMessage = "Street Name Required")]
        [RegularExpression(@"^[a-zA-Z\s]+$", ErrorMessage = "Street name only accepts letters a-z or A-Z!")]
        [StringLength(30)]
        public String StreetName { get; set; }
        [Required(ErrorMessage = "City Required")]
        [RegularExpression(@"^[a-zA-Z\s]+$", ErrorMessage = "City only accepts letters a-z or A-Z!")]
        [StringLength(30)]
        public String City { get; set; }
        [Required(ErrorMessage = "Province Required")]
        [RegularExpression(@"^[a-zA-Z\s]+$", ErrorMessage = "Province only accepts letters a-z or A-Z!")]
        [StringLength(2)]
        public String Province { get; set; }
        [Required(ErrorMessage = "Postal Code Required")]
        [RegularExpression(@"^([a-zA-Z]\d[a-zA-Z]( )?\d[a-zA-Z]\d)$", ErrorMessage = "Please enter a valid postal code! Format: A0A 0A0")]
        [StringLength(7)]
        public String PostalCode { get; set; }
        [Required(ErrorMessage = "Country Required")]
        [StringLength(30)]
        public String Country { get; set; }
        [Required(ErrorMessage = "Phone Number Required")]
        [Phone(ErrorMessage = "Please enter a valid phone number! Format: (xxx) xxx-xxxx")]
        [StringLength(14)]
        public String PhoneNumber { get; set; }
        [Required(ErrorMessage = "Email Required")]
        [EmailAddress(ErrorMessage = "Please enter a valid e-mail address!")]
        [StringLength(50)]
        public String Email { get; set; }

    }
}