﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Project.ViewModels;

namespace Project.Models
{
    public class CheckOutGreaterCheckIn : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var reservation = (Reservation)validationContext.ObjectInstance;

            return (reservation.CheckOutDate > reservation.CheckInDate)
                ? ValidationResult.Success
                : new ValidationResult("Check out date must be greater than check in date");

        }
    }
}