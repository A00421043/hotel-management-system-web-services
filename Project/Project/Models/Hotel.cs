﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project.Models
{
    public class Hotel
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String Address { get; set; }
    }
}