﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Project.ViewModels;

namespace Project.Models
{
    public class CheckInDateValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var reservation = (Reservation) validationContext.ObjectInstance;

            return (reservation.CheckInDate > DateTime.Today)
                ? ValidationResult.Success
                : new ValidationResult("Check in date must be in the future!");

        }
    }
}