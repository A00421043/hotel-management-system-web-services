﻿using Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Project.ViewModels;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;


namespace Project.Controllers
{
    public class ReservationController : Controller
    {
        private ApplicationDbContext _DbContext;

        public ReservationController()
        {
            _DbContext = new ApplicationDbContext();
        }

        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to Hotel Halifax.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Welcome to Hotel Halifax. This section will be updated soon.";

            return View();
        }

        public ActionResult Contact()
        {

            return View();
        }

        public ActionResult ReservationPage()
        {
            var viewModel = new ViewModelReservationPage();
            viewModel.Hotels = _DbContext.Hotels;
            return View(viewModel);
        }
        //GET: Reservation



        public ActionResult ReservationInfo(ViewModelReservationPage reservationInfo)
        {

            //  String body = JsonConvert.SerializeObject(_DbContext.Hotels.Single(r=>r.Id==1));
            var webReq = System.Net.WebRequest.Create("http://dev.cs.smu.ca:8093/Hotel_Reservation_WebService_API/rest/Hotels") as
                    HttpWebRequest;
            //var webReq = System.Net.WebRequest.Create("http://localhost:8080/Hotel_Reservation_WebService_API/rest/Reservations")
            //        as HttpWebRequest;

            if (!ModelState.IsValid)
            {
                var viewModel = new ViewModelReservationPage
                {
                    CreditCard = reservationInfo.CreditCard,
                    Customer = reservationInfo.Customer,
                    Reservation = reservationInfo.Reservation,
                    Hotels = _DbContext.Hotels.ToList()
                };
                return View("ReservationPage", viewModel);
            }

            var reservation = reservationInfo.Reservation;
            reservation.Customer = reservationInfo.Customer;
            reservation.CreditCard = reservationInfo.CreditCard;
            reservation.AgentId = User.Identity.GetUserId();
            String reservationString = "";
            String MethodName = "PUT";
            if (reservationInfo.Id == 0)
            {
                MethodName = "POST";
                reservation.ReservationStatus = "Active";
                _DbContext.Reservations.Add(reservation);
            }
            else
            {
                // var reserveInDb = _DbContext.Reservations.Single(r => r.ReservationId == reservationInfo.Id);
                var resId = reservationInfo.Id ?? default(int);
                var reserveInDb = GetReservation(resId);
                reserveInDb.ReservationStatus = reservationInfo.Reservation.ReservationStatus;
                reserveInDb.CheckInDate = reservationInfo.Reservation.CheckInDate;
                reserveInDb.CheckOutDate = reservationInfo.Reservation.CheckOutDate;
                reserveInDb.ReservationStatus = reservationInfo.Reservation.ReservationStatus;

                reserveInDb.CreditCard.CreditCardName = reservationInfo.Reservation.CreditCard.CreditCardName;
                reserveInDb.CreditCard.CreditCardNumber = reservationInfo.Reservation.CreditCard.CreditCardNumber;
                reserveInDb.CreditCard.CreditCardType = reservationInfo.Reservation.CreditCard.CreditCardType;


                reserveInDb.Customer.FirstName = reservationInfo.Reservation.Customer.FirstName;
                reserveInDb.Customer.LastName = reservationInfo.Reservation.Customer.LastName;
                reserveInDb.Customer.StreetNumber = reservationInfo.Reservation.Customer.StreetNumber;
                reserveInDb.Customer.StreetName = reservationInfo.Reservation.Customer.StreetName;
                reserveInDb.Customer.City = reservationInfo.Reservation.Customer.City;
                reserveInDb.Customer.Province = reservationInfo.Reservation.Customer.Province;
                reserveInDb.Customer.PostalCode = reservationInfo.Reservation.Customer.PostalCode;
                reserveInDb.Customer.Country = reservationInfo.Reservation.Customer.Country;
                reserveInDb.Customer.PhoneNumber = reservationInfo.Reservation.Customer.PhoneNumber;
                reserveInDb.Customer.Email = reservationInfo.Reservation.Customer.Email;
                reservationInfo.Reservation.ReservationId = reservationInfo.Id ?? default(int);
                reservationString = JsonConvert.SerializeObject(reserveInDb);

            }
            _DbContext.SaveChanges();
            //Code to add reservation in database
            if (MethodName == "POST")
            {
                reservationString = JsonConvert.SerializeObject(reservation);
            }

            String customerString = JsonConvert.SerializeObject(reservation.Customer);
            String creditCardString = JsonConvert.SerializeObject(reservation.CreditCard);
            webReq = System.Net.WebRequest.Create(
                "http://dev.cs.smu.ca:8093/Hotel_Reservation_WebService_API/rest/Reservations") as HttpWebRequest;

            //webReq = System.Net.WebRequest.Create(
            //    "http://localhost:8080/Hotel_Reservation_WebService_API/rest/Reservations") as HttpWebRequest;
            if (webReq != null)
            {
                StreamWriter requestWriter;
                webReq.Method = MethodName;
                webReq.ServicePoint.Expect100Continue = false;
                webReq.Timeout = 20000;
                webReq.ContentType = "application/json";

                using (requestWriter = new StreamWriter(webReq.GetRequestStream()))
                {
                    requestWriter.Write(reservationString);
                }
                Console.WriteLine();
                HttpWebResponse resp = (HttpWebResponse)webReq.GetResponse();
                Stream resStream = resp.GetResponseStream();
                StreamReader reader = new StreamReader(resStream);
                reader.ReadToEnd();
            }

            if (MethodName == "POST")
            {
                return RedirectToAction("ReservationSuccess", "Reservation");
            }
            else
            {
                return RedirectToAction("ReservationEditSuccess", "Reservation");
            }

        }

        public ActionResult ReservationSuccess()
        {
            return View();
        }

        public ActionResult ReservationSuccessInfo()
        {
            return RedirectToAction("Index", "Reservation");
        }

        public ActionResult ReservationEditSuccess()
        {
            return View();
        }

        public ActionResult ReservationEditSuccessInfo()
        {
            return RedirectToAction("ReservationList", "Reservation");
        }

        //To Cancel a reservation
        public ActionResult CancelReservation(int id)
        {
            //String agentId = User.Identity.GetUserId();
            var reservation = GetReservation(id);

            if (reservation == null)
            {
                return HttpNotFound();
            }
            reservation.ReservationStatus = "Cancel";
            _DbContext.SaveChanges();

            //Connect to Mysql Db and cancel


            var webReq =
                System.Net.WebRequest.Create(
                    "http://dev.cs.smu.ca:8093/Hotel_Reservation_WebService_API/rest/Reservations/" + id) as HttpWebRequest;
            if (webReq != null)
            {
                StreamWriter requestWriter;
                webReq.Method = "DELETE";
                webReq.ServicePoint.Expect100Continue = false;
                webReq.Timeout = 20000;
                webReq.ContentType = "application/json";
                var reservationString = JsonConvert.SerializeObject(reservation);
                using (requestWriter = new StreamWriter(webReq.GetRequestStream()))
                {
                    requestWriter.Write(reservationString);
                }
                Console.WriteLine();
                HttpWebResponse resp = (HttpWebResponse)webReq.GetResponse();
                Stream resStream = resp.GetResponseStream();
                StreamReader reader = new StreamReader(resStream);
                reader.ReadToEnd();
            }
            return RedirectToAction("ReservationList", "Reservation");
        }

        public ActionResult ReservationList()
        {
            //var reservations = _DbContext.Reservations.Where(r => r.AgentId == agentId).ToList();
            //String agentId = User.Identity.GetUserId();
            List<Reservation> reservations = new List<Reservation>();
            string getAllURL = "http://dev.cs.smu.ca:8093/Hotel_Reservation_WebService_API/rest/Reservations";
            //string getAllURL = "http://localhost:8080/Hotel_Reservation_WebService_API/rest/Reservations";
            string urlParameters = "";

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(getAllURL);


            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result; // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!

                var dataObjects = response.Content.ReadAsAsync<IEnumerable<Reservation>>().Result;
                foreach (var d in dataObjects)
                {
                    //Console.WriteLine("{0}", d.CheckInDate);
                    reservations.Add(d);

                }
                return View(reservations);
            }
            else
            {
                //Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }
            return View(reservations);
        }

        public ActionResult ReservationDetail(int id)
        {

            String agentId = User.Identity.GetUserId();

            string getAllURL = "http://dev.cs.smu.ca:8093/Hotel_Reservation_WebService_API/rest/Reservations/" + id;
            string urlParameters = "";

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(getAllURL);


            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result; // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!

                Reservation reservationInstance = response.Content.ReadAsAsync<Reservation>().Result;
                return View(reservationInstance);
            }
            else
            {
                //Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }



            var reservation = _DbContext.Reservations.Where(r => r.AgentId == agentId)
                .SingleOrDefault(r => r.ReservationId == id);
            if (reservation == null)
            {
                return HttpNotFound();
            }
            return View(reservation);
        }

        public ActionResult Edit(int id)
        {
            //String agentId = User.Identity.GetUserId();
            //var reservation = _DbContext.Reservations.Where(r => r.AgentId == agentId)
            //    .SingleOrDefault(r => r.ReservationId == id);
            var reservation = GetReservation(id);
            if (reservation == null)
            {
                return HttpNotFound();
            }
            var customer = reservation.Customer;
            var creditCard = reservation.CreditCard;
            //var hotels = _DbContext.CreditCards.SingleOrDefault(r => r.CreditCardId == reservation.CreditCardId);
            var viewModel = new ViewModelReservationPage(creditCard, customer, reservation);
            viewModel.Hotels = _DbContext.Hotels;
            //viewModel.Id = reservation.ReservationId;


            return View("ReservationPage", viewModel);
        }

        public Reservation GetReservation(int? id)
        {
            //Code to add reservation in database
            if (id == null)
            {
                return null;
            }
           // String agentId = User.Identity.GetUserId();

            string getAllURL = "http://dev.cs.smu.ca:8093/Hotel_Reservation_WebService_API/rest/Reservations/" + id;
            string urlParameters = "";

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(getAllURL);


            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result; // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!

                Reservation reservationInstance = response.Content.ReadAsAsync<Reservation>().Result;
                return reservationInstance;
            }
            return null;
        }

        
    }

}

