﻿using Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace Project.Controllers
{
    public class HotelsController : Controller
    {
        public List<Hotel> GetHotels()
        {
            //Code to add reservation in database
            //string getAllURL = "http://dev.cs.smu.ca:8093/Hotel_Reservation_WebService_API/rest/Hotels/";
            string getAllURL = "http://dev.cs.smu.ca:8093/Hotel_Reservation_WebService_API/rest/Hotels/";
            string urlParameters = "";

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(getAllURL);


            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result; // Blocking call!
            List<Hotel> hotels = new List<Hotel>();
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!

                var dataObjects = response.Content.ReadAsAsync<IEnumerable<Hotel>>().Result;
                foreach (var d in dataObjects)
                {
                    //Console.WriteLine("{0}", d.CheckInDate);
                    hotels.Add(d);

                }
                return hotels;
            }
            return null;
        }

    }
}