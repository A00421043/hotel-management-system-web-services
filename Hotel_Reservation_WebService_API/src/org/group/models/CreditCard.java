package org.group.models;

public class CreditCard {
	
	private int CreditCardId;
	private String CreditCardType;
	private String CreditCardName;
	private String CreditCardNumber;
	private String ExpiryDate;

	public CreditCard( ) {

	}	
	
	public CreditCard( int CreditCardId, String CreditCardType,String CreditCardName ,String CreditCardNumber,String ExpiryDate   ) {
		this.CreditCardId=CreditCardId;
		this.CreditCardType = CreditCardType;
		this.CreditCardName = CreditCardName;
		this.CreditCardNumber=CreditCardNumber;
		this.ExpiryDate=ExpiryDate; 
	
	}

	public int getCreditCardId() {
		return CreditCardId;
	}
	public void setCreditCardId(int id) {
		this.CreditCardId = id;
	}
	
	public String getCreditCardType() {
		return CreditCardType;
	}
	public void setCreditCardType(String creditcardtype) {
		this.CreditCardType = creditcardtype;
	}
	
	public String getCreditCardName() {
		return CreditCardName;
	}
	public void setCreditCardName(String creditcardname) {
		this.CreditCardName = creditcardname;
	}
	
	public String getCreditCardNumber() {
		return CreditCardNumber;
	}
	public void setCreditCardNumber(String creditcardnumber) {
		this.CreditCardNumber = creditcardnumber;
	}
	
	public String getExpiryDate() {
		return ExpiryDate;
	}
	public void setExpiryDate(String expirydate) {
		this.ExpiryDate = expirydate;
	}
	
	
}
