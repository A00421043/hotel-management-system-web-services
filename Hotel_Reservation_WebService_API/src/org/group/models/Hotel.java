package org.group.models;

public class Hotel {

		private int Id ;
	    private String Name ;
	    private String Address ;
		public Hotel() {
			
		}
		
		public Hotel(int id, String name, String address) {
			this.Id = id;
			this.Name = name;
			this.Address = address;
			
		}
	    
	    public int getId() {
			return Id;
		}
		public void setId(int id) {
			Id = id;
		}
		public String getName() {
			return Name;
		}
		public void setName(String name) {
			Name = name;
		}
		public String getAddress() {
			return Address;
		}
		public void setAddress(String address) {
			Address = address;
		}
	}

