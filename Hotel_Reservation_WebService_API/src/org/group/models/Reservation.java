package org.group.models;

//import java.util.Date;

public class Reservation {
	
	private int ReservationId;
	private int customerId;
	private String checkInDate;
	private String CheckOutDate;
	private String RoomNumber;
	private String reservationStatus;
	private Customer customer;
	private CreditCard creditCard;
	private Hotel hotel;
	private int HotelId;
	private int guests;
	private int CreditCardId;
	private String AgentId;
	
	
	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
	public int getGuests() {
		return guests;
	}

	public void setGuests(int guests) {
		this.guests = guests;
	}

	public String getAgentId() {
		return AgentId;
	}

	public void setAgentId(String agentId) {
		AgentId = agentId;
	}

	public Reservation( ) {

	}	
	
	public int getHotelId() {
		return HotelId;
	}
	
	public void setHotelId(int hotelId) {
		HotelId = hotelId;
	}
	
	
	public int getReservationId() {
		return ReservationId;
	}
	
	public void setReservationId(int reservationId) {
		ReservationId = reservationId;
	}
	public int getCustomerId() {
		return this.customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getCheckInDate() {
		return checkInDate;
	}
	
	public void setCheckInDate(String checkInDate) {
		this.checkInDate = checkInDate;
	}
	
	public String getCheckOutDate() {
		return CheckOutDate;
	}
	public void setCheckOutDate(String checkOutDate) {
		CheckOutDate = checkOutDate;
	}
	public String getRoomNumber() {
		return RoomNumber;
	}
	public void setRoomNumber(String roomNumber) {
		RoomNumber = roomNumber;
	}
	public String getReservationStatus() {
		return reservationStatus;
	}
	public void setReservationStatus(String reservationStatus) {
		this.reservationStatus = reservationStatus;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public CreditCard getCreditCard() {
		return creditCard;
	}
	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}
	public int getCreditCardId() {
		return CreditCardId;
	}
	public void setCreditCardId(int creditCardId) {
		CreditCardId = creditCardId;
	}
}
