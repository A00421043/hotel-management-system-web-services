package org.group.models;

public class Customer {
	

	private int customerId;
	private String firstName;
	private String LastName;
	private String StreetNumber;
	private String streetName;
	private String City;
	private String province; 
	private String PostalCode;
	private String Country;
	private String PhoneNumber;
	private String email;
	public Customer( ) {

	}	
	public void printCustomer()
	{
		System.out.println(this.getFirstName());
		System.out.println(this.getLastName());
		System.out.println(this.getStreetNumber());
		System.out.println(this.getStreetName());
		System.out.println(this.getCity());
		System.out.println(this.getProvince());
		System.out.println(this.getPostalCode());
		System.out.println(this.getCountry());
		System.out.println(this.getPhoneNumber());
		System.out.println(this.getEmail());
	}
	public Customer( int id, String firstname,String lastname ,String StreetNumber,String StreetName,String City,String Province ,String PostalCode,String Country,String PhoneNumber ,String email    ) {
		this.customerId=id;
		this.firstName = firstname;
		this.LastName = lastname;
		this.StreetNumber=StreetNumber;
		this.streetName=StreetName;
		this.City = City;
		this.province = Province;
		this.PostalCode = PostalCode;
		this.Country = Country;
		this.PhoneNumber=PhoneNumber;
		this.email = email;
	}
	
	
	
	
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		this.LastName = lastName;
	}
	
	public void setStreetNumber(String streetNumber){
		
		this.StreetNumber = streetNumber;
	}
	
	public String getStreetNumber(){
		
		return StreetNumber;
	}
	
	public void setStreetName(String streetName){
		
		this.streetName = streetName;
	}
	
	public String getStreetName(){
		
		return streetName;
	}
	
	
	public void setCity(String City){
		
		this.City = City;
	}
	
	public String getCity(){
		
		return City;
	}
	
public void setPostalCode(String postal){
		
		this.PostalCode = postal;
	}
	
	public String getPostalCode(){
		
		return PostalCode;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

	public String getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
	
}
