package org.group.controllers;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.group.dao.CreditCardDAO;
import org.group.dao.CustomerDAO;
import org.group.dao.HotelDAO;
import org.group.dao.ReservationDAO;
import org.group.jdbcDAO.JDBCreditCardDAO;
import org.group.jdbcDAO.JDBCustomerDAO;
import org.group.jdbcDAO.JDBHotelDAO;
import org.group.jdbcDAO.JDBReservationDAO;
import org.group.models.Reservation;

@Path("/Reservations")
public class ReservationController {

	
	@GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<Reservation> getReservation() {
    	ReservationDAO dao = new JDBReservationDAO();
    	
        
    	return dao.getAllReservation();
    }
	
	
    // URI:
    // /contextPath/servletPath/Reservations/{id}
    @GET
    @Path("/{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Reservation getReservation(@PathParam("id") int id) {
    	ReservationDAO dao = new JDBReservationDAO();
    	CustomerDAO customerDAO=new JDBCustomerDAO();
    	CreditCardDAO creditCardDAO=new JDBCreditCardDAO();
    	Reservation reservation=dao.getReservation(id);
    	HotelDAO hotelDao = new JDBHotelDAO();
    	System.out.println(reservation.getCustomerId());
    	System.out.println("Customer Id"+reservation.getCustomerId());
    	reservation.setHotel(hotelDao.getHotel(reservation.getHotelId()));
    	reservation.setCustomer(customerDAO.getCustomer(reservation.getCustomerId()));
    	reservation.setCreditCard(creditCardDAO.getCreditCard(reservation.getCreditCardId()));
    	
        return reservation;
    }
 
    // URI:
    // /contextPath/servletPath/Reservations
    @POST
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Consumes(MediaType.APPLICATION_JSON)
    public Reservation addReservation(Reservation reservation) {
    	int customerId=0;
    	int creditCardId=0;
    	ReservationDAO dao = new JDBReservationDAO();
    	CustomerDAO customerDAO=new JDBCustomerDAO();
    	CreditCardDAO creditCardDAO=new JDBCreditCardDAO();
    	
    	customerId=customerDAO.addCustomer(reservation.getCustomer());
    	creditCardId=creditCardDAO.addCreditCard(reservation.getCreditCard());
    	reservation.setCustomerId(customerId);
    	reservation.setCreditCardId(creditCardId);
    	
    	return dao.addReservation(reservation);
    }
 
    // URI:
    // /contextPath/servletPath/Reservations
    @PUT
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Reservation updateReservation(Reservation reservation) {
    	ReservationDAO dao = new JDBReservationDAO();
    	
        return dao.updateReservation(reservation);
    }
 
    @DELETE
    @Path("/{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public void deleteReservation(@PathParam("id") int id) {
    	ReservationDAO dao = new JDBReservationDAO();
    	System.out.println("I am in cancel");
    	dao.deleteReservation(id);
    }	
}

