package org.group.controllers;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.group.dao.CreditCardDAO;
import org.group.jdbcDAO.JDBCreditCardDAO;
import org.group.models.CreditCard;

@Path("/Creditcards")
public class CreditCardController {

	@GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<CreditCard> getCreditCard() {
    	CreditCardDAO dao = new JDBCreditCardDAO();
    	
        
    	return dao.getAllCreditCard();
    }
	
    // URI:
    // /contextPath/servletPath/CreditCards/{id}
    @GET
    @Path("/{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public CreditCard getCreditCard(@PathParam("id") int id) {
    	CreditCardDAO dao = new JDBCreditCardDAO();
        return dao.getCreditCard(id);
    }
 
    // URI:
    // /contextPath/servletPath/CreditCards
    @POST
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public int addCreditCard(CreditCard card) {
    	CreditCardDAO dao = new JDBCreditCardDAO();
        System.out.println(card.getCreditCardId()+ " "+ card.getCreditCardName()+ " "+ card.getCreditCardNumber() + " "+card.getCreditCardType()+ " "+ card.getExpiryDate());
    	return dao.addCreditCard(card);
        
    }
 
    // URI:
    // /contextPath/servletPath/CreditCards
    @PUT
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public CreditCard updateCreditCard(CreditCard card) {
    	CreditCardDAO dao = new JDBCreditCardDAO();
        return dao.updateCreditCard(card);
    }
 
    @DELETE
    @Path("/{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public void deleteCreditCard(@PathParam("id") int id) {
    	CreditCardDAO dao = new JDBCreditCardDAO();
    	dao.deleteCreditCard(id);
    }	
}

