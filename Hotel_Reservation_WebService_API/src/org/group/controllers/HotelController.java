package org.group.controllers;


import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.group.dao.CustomerDAO;
import org.group.dao.HotelDAO;
import org.group.jdbcDAO.JDBCustomerDAO;
import org.group.jdbcDAO.JDBHotelDAO;
import org.group.models.Hotel;

@Path("/Hotels")
public class HotelController {
	
    // URI:
    // /contextPath/servletPath/Hotels
    @GET
    @Produces({ MediaType.APPLICATION_JSON})
    public List<Hotel> getHotels_JSON() {
    	HotelDAO dao = new JDBHotelDAO();
        List<Hotel> listOfHotels = dao.getAllHotels();
        return listOfHotels;
    }
    

    // URI:
    // /contextPath/servletPath/Hotels/{id}
    @GET 
    @Path("/{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Hotel getHotel(@PathParam("id") int id) {
    	HotelDAO dao = new JDBHotelDAO();
    	return dao.getHotel(id);
    }
 
    // URI:
    // /contextPath/servletPath/Hotels
    @POST
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Consumes(MediaType.APPLICATION_JSON)
    public Hotel addHotel(Hotel hotel) {
    	HotelDAO dao = new JDBHotelDAO();
    	System.out.println(hotel.getId()+" "+hotel.getName()+" "+ hotel.getAddress() );
       return dao.addHotel(hotel);
    }
 
    // URI:
    // /contextPath/servletPath/Customers
    @PUT
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Hotel updateHotel(Hotel hotel) {
    	HotelDAO dao = new JDBHotelDAO();
        return dao.updateHotel(hotel);
    }
 
    @DELETE
    @Path("/{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public void deleteEmployee(@PathParam("id") int id) {
    	CustomerDAO dao = new JDBCustomerDAO();
    	dao.deleteCustomer(id);
    }
 

}
