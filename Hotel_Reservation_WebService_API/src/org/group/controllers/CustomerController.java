package org.group.controllers;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.group.dao.CustomerDAO;
import org.group.jdbcDAO.JDBCustomerDAO;
import org.group.models.Customer;

@Path("/Customers")
public class CustomerController {

    // URI:
    // /contextPath/servletPath/Customers
    @GET
    @Produces({ MediaType.APPLICATION_JSON})
    public List<Customer> getEmployees_JSON() {
    	CustomerDAO dao = new JDBCustomerDAO();
        List<Customer> listOfCustomers = dao.getAllCustomers();
        System.out.println("GOT IT!!!!!!!!!1");
        return listOfCustomers;
        
    }
 
	/*
	@GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Customer getCustomer() {
    	CustomerDAO dao = new InMemoryCustomerDAO();
        
    	return dao.getCustomer(0);
    }
	*/
	
	
	
    // URI:
    // /contextPath/servletPath/Customers/{id}
    @GET 
    @Path("/{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Customer getCustomer(@PathParam("id") int id) {
    	CustomerDAO dao = new JDBCustomerDAO();
    	
    	return dao.getCustomer(id);
    }
 
    // URI:
    // /contextPath/servletPath/Customers
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public int addCustomer(Customer cust) {
    	CustomerDAO dao = new JDBCustomerDAO();
        return dao.addCustomer(cust);
    }
 
    // URI:
    // /contextPath/servletPath/Customers
    @PUT
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Customer updateCustomer(Customer cust) {
    	CustomerDAO dao = new JDBCustomerDAO();
        return dao.updateCustomer(cust);
    }
 
    @DELETE
    @Path("/{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public void deleteEmployee(@PathParam("id") int id) {
    	CustomerDAO dao = new JDBCustomerDAO();
    	dao.deleteCustomer(id);
    }	
}