package org.group.global;

import java.sql.DriverManager;

import com.mysql.jdbc.Connection;

public class Connect_DB {        
    static Connection con=null;

    public static Connection getConnection()
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con=(Connection) DriverManager.getConnection("jdbc:mysql://localhost/mcda551002?user=root&password=root");
            //con=(Connection) DriverManager.getConnection("jdbc:mysql://cs.smu.ca:3306/mcda551002?user=mcda551002&password=mcda551002password");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        return con;        
    }
}