package org.group.dao;
import java.util.List;

import org.group.models.Reservation;;

public interface  ReservationDAO {


	Reservation getReservation(int id);

	Reservation addReservation(Reservation reserve);

	Reservation updateReservation(Reservation reserve);

	void deleteReservation(int id);

	List<Reservation> getAllReservation();
	

}