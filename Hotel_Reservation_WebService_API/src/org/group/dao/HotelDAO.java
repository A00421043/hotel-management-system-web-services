package org.group.dao;

import java.util.List;

import org.group.models.Hotel;

public interface HotelDAO {
	Hotel getHotel(int id);

	Hotel addHotel(Hotel hotel);

	Hotel updateHotel(Hotel hotel);

	//void deleteHotel(int id); //Reminder by Maroof: Talk to Nitin & Janti whether we need delete

	List<Hotel> getAllHotels();
}
