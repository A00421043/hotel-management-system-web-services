package org.group.dao;

import java.util.List;

import org.group.models.Customer;

public interface CustomerDAO {

	Customer getCustomer(int id);

	int addCustomer(Customer cust);

	Customer updateCustomer(Customer cust);

	void deleteCustomer(int id);

	List<Customer> getAllCustomers();

}