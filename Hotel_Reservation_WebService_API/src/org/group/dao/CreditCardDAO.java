package org.group.dao;
import java.util.List;

import org.group.models.CreditCard;;

public interface  CreditCardDAO {


	CreditCard getCreditCard(int id);

	int addCreditCard(CreditCard card);

	CreditCard updateCreditCard(CreditCard card);

	void deleteCreditCard(int id);

	List<CreditCard> getAllCreditCard();
	

}