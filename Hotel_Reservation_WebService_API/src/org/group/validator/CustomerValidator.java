package org.group.validator;


public class CustomerValidator {
	public String customerchk ="";
	
	public String regexEmail = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
	public String reqularExpNumber = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$";
	public String regexStrNumber ="\\b[1-9][0-9]+\\b";
	
	public String regexNames = "[a-zA-Z]";
	
	public String regexPostalCode = "^(?i)(\\d{5}(-\\d{4})?|[A-CEGHJ-NPRSTVXY]\\d[A-CEGHJ-NPRSTV-Z] ?\\d[A-CEGHJ-NPRSTV-Z]\\d)$";
	

	public String chkCustomer (String fname, String lname , String StreetNumber, String StreetName, String City, String Province, String PostalCode, String Country, String PhoneNumber, String Email )
	{
	if (!fname.matches(regexNames)  || !lname.matches(regexNames) || fname.isEmpty() || lname.isEmpty() || fname.length() > 50 || lname.length() > 50)
	{  
		customerchk = "Customer First Name or Last Name has invalid characters or null or the length is greater than 50"; 
		return customerchk;
		}
	
	
	else if (StreetName.length() > 50 || StreetName.isEmpty() || !StreetName.matches(regexNames))
	{ customerchk = "Street Name is empty or has greater than 50 characters";
	return customerchk;
	}
	
	else if (StreetNumber.length() > 4 || !StreetNumber.trim().matches(regexStrNumber))
	{ customerchk = "Street Number is Invalid. Please check you have inputted numbers only and length <= 4";
	return customerchk;
	}
	
	else if (!City.matches(regexNames) || City.isEmpty() || City.length() > 15)
	{   customerchk ="City has invalid Character or is null ";
	return customerchk;
	}
	
	else if (!PostalCode.matches(regexPostalCode))
	{ customerchk = "Postal or Zip is not in the right format";
	return customerchk;
	}
	
	else if (!PhoneNumber.matches(reqularExpNumber))
	{customerchk ="Phone number not in the required format"; 
	return customerchk;
	}
	
	else if (!Email.matches(regexEmail))
	{customerchk ="Email is in improper format";
	
	return customerchk; 
	}
	
	else if (Country.toLowerCase().trim() != "canada" || Country.toLowerCase().trim() !="united states" ||Country.isEmpty())
	{
		customerchk = "Country Should be CANADA or UNITED STATES";
		return customerchk;
	}
		
	customerchk = "Customer Data is Okay!!";
		return customerchk ;}
	


}
