package org.group.validator;

public class CreditCardValidator {
	public String cardChk;
	
	
	public String regExpAMEX = "^3[47][0-9]{13}$";
	
	public String regExpVISAMAster = "^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14})$";

	public String chkCard(String PAN, String ExpDt) {
		if (!PAN.isEmpty()) {
			if (PAN.length() == 16)

				if (!PAN.matches(regExpVISAMAster)) {
					cardChk = "Card PAN is invalid --VISA/MASTERCARD ERROR";
					return cardChk;
				}

			if (PAN.length() == 15) {
				if (!PAN.matches(regExpAMEX)) {
					cardChk = "Card PAN is invalid --AMEX Error";

					return cardChk;
				}

			}

		}
		if (!ExpDt.trim().isEmpty() && ExpDt.trim().length() == 7) {
			if (ExpDt.substring(3, 0) != "/") {
				cardChk = "The required slash is not in the Expiry date";
				return cardChk;
			}
			if (Integer.parseInt(ExpDt.substring(3)) <= 2016 && Integer.parseInt(ExpDt.substring(3)) >= 2031) {
				cardChk = "Card has expired or not yet issued";
				return cardChk;
			}
		} 
cardChk = "Card is okay";
		return cardChk;
	}

}
