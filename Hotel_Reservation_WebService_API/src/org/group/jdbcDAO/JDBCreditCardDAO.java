package org.group.jdbcDAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import org.group.dao.CreditCardDAO;
import org.group.global.Connect_DB;
import org.group.models.CreditCard;

public class JDBCreditCardDAO implements CreditCardDAO{
	
	@Override
	public int addCreditCard(CreditCard card) {
		int creditCardId=0;
        try {
        	//This is just testing
        	String queryString = String.format("INSERT INTO `mcda551002`.`CreditCards`\r\n" + 
            		//"(`CreditCardId`,\r\n" + 
            		"(`CreditCardType`,\r\n" + 
            		"`CreditCardName`,\r\n" + 
            		"`CreditCardNumber`,\r\n" + 
            		"`ExpiryDate`)\r\n" + 
            		"VALUES\r\n" + 
            		"(%s,%s,%s,%s)","'"+ card.getCreditCardType()+"'","'"+card.getCreditCardName()+"'",
            		"'"+card.getCreditCardNumber()+"'","'"+card.getExpiryDate()+"'");
        	System.out.println(queryString);
            PreparedStatement preparedStatement = Connect_DB.getConnection().prepareStatement(queryString,Statement.RETURN_GENERATED_KEYS);
            preparedStatement.executeUpdate();
           // preparedStatement.getGeneratedKeys().
           
            ResultSet keys = preparedStatement.getGeneratedKeys();    
            keys.next();  
            creditCardId=keys.getInt(1);
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return creditCardId;
         
    }

	@Override   
    public List<CreditCard> getAllCreditCard() {
        List<CreditCard> cards = new LinkedList<CreditCard>();
         try {
                Statement statement = Connect_DB.getConnection().createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT * FROM `mcda551002`.CreditCards");
                 
                CreditCard card = null;
                while(resultSet.next()){
                	card = new CreditCard();
                	card.setCreditCardId(Integer.parseInt(resultSet.getString("CreditCardId")));
                	card.setCreditCardType(resultSet.getString("CreditCardType"));
                	card.setCreditCardName(resultSet.getString("CreditCardName"));
                	card.setCreditCardType(resultSet.getString("CreditCardNumber"));
                	card.setExpiryDate(resultSet.getString("ExpiryDate"));
                    cards.add(card);
                }
                
                
                resultSet.close();
                statement.close();
                 
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return cards;
    }
	
	@Override
	public CreditCard getCreditCard(int id) {

        List<CreditCard> cards = new LinkedList<CreditCard>();
		try {
               Statement statement = Connect_DB.getConnection().createStatement();
               ResultSet resultSet = statement.executeQuery("SELECT * FROM `mcda551002`.CreditCards WHERE CreditCardId="+id);
                
               CreditCard card = null;
               while(resultSet.next()){
            	   card = new CreditCard();
            	   card.setCreditCardId(Integer.parseInt(resultSet.getString("CreditCardId")));
            	   card.setCreditCardType(resultSet.getString("CreditCardType"));
            	   card.setCreditCardName(resultSet.getString("CreditCardName"));
            	   card.setCreditCardNumber(resultSet.getString("CreditCardNumber"));
            	   card.setExpiryDate(resultSet.getString("ExpiryDate"));
                
            	   cards.add(card);  
               }
               
               resultSet.close();
               statement.close();
               return card;
                
			} catch (SQLException e) {
               e.printStackTrace();
           }
           return null;
	}
	
	@Override
	public CreditCard updateCreditCard(CreditCard card) {
		  int id = card.getCreditCardId();
			try {
	              // Statement statement = getConnection().createStatement();
	              
				String queryString = String.format("UPDATE `mcda551002`.CreditCards SET CreditCardType='%s', CreditCardName ='%s',"
	               		+ "CreditCardNumber='%s',ExpiryDate='%s WHERE CreditCardId ="+id, card.getCreditCardType(), 
	               		card.getCreditCardName(), card.getCreditCardNumber(), card.getExpiryDate()+"'");
	               
				System.out.println(queryString);
				PreparedStatement statement = Connect_DB.getConnection().prepareStatement(queryString);
				statement.executeUpdate();   
				statement.close();
	                
				} catch (SQLException e) {
	               e.printStackTrace();
	           }
	           return null;
		
	}
	
	@Override
	public void deleteCreditCard(int CreditCardId) {
		/*
		PreparedStatement preparedStatement = getConnection().prepareStatement(queryString);
        preparedStatement.executeUpdate();
		*/
	}

}
