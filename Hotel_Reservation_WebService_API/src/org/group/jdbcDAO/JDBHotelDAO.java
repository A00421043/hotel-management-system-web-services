package org.group.jdbcDAO;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import org.group.dao.HotelDAO;
import org.group.global.Connect_DB;
import org.group.models.Hotel;

public class JDBHotelDAO implements HotelDAO {
	
	public Hotel getHotel(int id) {

        List<Hotel> hotels = new LinkedList<Hotel>();
		try {
               Statement statement = Connect_DB.getConnection().createStatement();
               ResultSet resultSet = statement.executeQuery("SELECT * FROM `mcda551002`.hotel WHERE id="+id);
                
               Hotel hotel = null;
               while(resultSet.next()){
               	hotel = new Hotel();
               	hotel.setId(Integer.parseInt(resultSet.getString("id")));
               	hotel.setName(resultSet.getString("name"));
                hotel.setAddress(resultSet.getString("address"));
                hotels.add(hotel);  
               }
               
               resultSet.close();
               statement.close();
               return hotel;
                
			} catch (SQLException e) {
               e.printStackTrace();
           }
           return null;

		
		
	}

	public Hotel addHotel(Hotel hotel) {
		
        try {
        	
        	String queryString = String.format("INSERT INTO `mcda551002`.`hotel`\r\n" + 
            		"(`id`,\r\n" + 
            		"`name`,\r\n" + 
            		"`address`)\r\n" + 
            		"VALUES\r\n" + 
            		"(%d,\'%s\',\'%s\')",hotel.getId(), hotel.getName(),hotel.getAddress() 
            		);
        	
        	System.out.println(queryString);
            PreparedStatement preparedStatement = Connect_DB.getConnection().prepareStatement(queryString);
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
        
	
	}

	public Hotel updateHotel(Hotel hotel) {
		
		 int id = hotel.getId();
			try {
	              // Statement statement = getConnection().createStatement();
	              
				String queryString = String.format("UPDATE `mcda551002`.hotel SET name='%s', address='%s' WHERE id ="+id, hotel.getName(), 
	               		hotel.getAddress());
	               
				System.out.println(queryString);
				PreparedStatement statement = Connect_DB.getConnection().prepareStatement(queryString);
	            //statement.executeQuery();
				statement.executeUpdate();   
				statement.close();
	                
				} catch (SQLException e) {
	               e.printStackTrace();
	           }
	           return null;

		
	}



	public List<Hotel> getAllHotels(){
	
		 List<Hotel> hotels = new LinkedList<Hotel>();
			try {
	               Statement statement = Connect_DB.getConnection().createStatement();
	               ResultSet resultSet = statement.executeQuery("SELECT * FROM `mcda551002`.hotel");
	                
	               Hotel hotel = null;
	               while(resultSet.next()){
	               	hotel = new Hotel();
	               	hotel.setId(Integer.parseInt(resultSet.getString("id")));
	               	hotel.setName(resultSet.getString("name"));
	                hotel.setAddress(resultSet.getString("address"));
	                hotels.add(hotel);  
	               }
	               
	               resultSet.close();
	               statement.close();
	               return hotels;
	                
				} catch (SQLException e) {
	               e.printStackTrace();
	           }
	           return null;

			
		
	}
}
