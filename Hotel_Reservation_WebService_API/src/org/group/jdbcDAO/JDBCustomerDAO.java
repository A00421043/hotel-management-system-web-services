package org.group.jdbcDAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import org.group.dao.CustomerDAO;
import org.group.global.Connect_DB;
import org.group.models.Customer;
public class JDBCustomerDAO implements CustomerDAO {

	@Override
	public int addCustomer(Customer customer) {
		int customerId=0;
        try {
        	
        	String queryString = String.format("INSERT INTO `mcda551002`.`customer`\r\n" + 
            		//"(`id`,\r\n" + 
            		"(`firstname`,\r\n" + 
            		"`lastname`,\r\n" + 
            		"`streetnumber`,\r\n" + 
            		"`streetname`,\r\n" + 
            		"`city`,\r\n" + 
            		"`province`,\r\n" + 
            		"`country`,\r\n" + 
            		"`email`,\r\n" + 
            		"`phone`,\r\n" + 
            		"`postalcode`)\r\n" + 
            		"VALUES\r\n" + 
            		"(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",/*customer.getCustomerId(),*/ "'"+customer.getFirstName()+"'", "'"+customer.getLastName()+ "'",
            		 "'"+customer.getStreetNumber()+ "'", "'"+customer.getStreetName()+ "'", "'"+ customer.getCity()+ "'",
            		 "'"+customer.getProvince()+ "'", "'"+customer.getCountry() +"'", "'"+customer.getEmail()+ "'",
            		 "'"+customer.getPhoneNumber()+ "'",  "'"+customer.getPostalCode()+ "'");
        	System.out.println(queryString);
            PreparedStatement preparedStatement = Connect_DB.getConnection().prepareStatement(queryString,Statement.RETURN_GENERATED_KEYS);
            preparedStatement.executeUpdate();
            ResultSet keys = preparedStatement.getGeneratedKeys();    
            keys.next();  
            customerId=keys.getInt(1);
            customer.setCustomerId(customerId);
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerId;
         
    }
 

	@Override   
    public List<Customer> getAllCustomers() {
        List<Customer> customers = new LinkedList<Customer>();
         try {
                Statement statement = Connect_DB.getConnection().createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT * FROM `mcda551002`.customer");
                 
                Customer customer = null;
                while(resultSet.next()){
                	customer = new Customer();
                	customer.setCustomerId(Integer.parseInt(resultSet.getString("id")));
                	customer.setFirstName(resultSet.getString("firstname"));
                    customer.setLastName(resultSet.getString("lastname"));
                    customer.setStreetNumber(resultSet.getString("streetnumber"));
                    customer.setStreetName(resultSet.getString("streetname"));
                    customer.setCity(resultSet.getString("city"));
                    customer.setProvince(resultSet.getString("province"));
                    customer.setCountry(resultSet.getString("country"));
                    customer.setPhoneNumber(resultSet.getString("phone"));
                    customer.setEmail(resultSet.getString("email"));
                    customer.setPostalCode(resultSet.getString("postalcode"));
                    customers.add(customer);
                }
                
                
                resultSet.close();
                statement.close();
                 
            } catch (SQLException e) {
                e.printStackTrace();
            }
            System.out.println(customers.get(0).getCity());
            return customers;
    }
     

	@Override
	public Customer getCustomer(int id) {

        List<Customer> customers = new LinkedList<Customer>();
		try {
               Statement statement = Connect_DB.getConnection().createStatement();
               ResultSet resultSet = statement.executeQuery("SELECT * FROM `mcda551002`.customer WHERE id="+id);
                
               Customer customer = null;
               while(resultSet.next()){
               	customer = new Customer();
               	customer.setCustomerId(Integer.parseInt(resultSet.getString("id")));
               	customer.setFirstName(resultSet.getString("firstname"));
                   customer.setLastName(resultSet.getString("lastname"));
                   customer.setStreetNumber(resultSet.getString("streetnumber"));
                   customer.setStreetName(resultSet.getString("streetname"));
                   customer.setCity(resultSet.getString("city"));
                   customer.setProvince(resultSet.getString("province"));
                   customer.setCountry(resultSet.getString("country"));
                   customer.setPhoneNumber(resultSet.getString("phone"));
                   customer.setEmail(resultSet.getString("email"));
                   customer.setPostalCode(resultSet.getString("postalcode"));
                
                   customers.add(customer);  
               }
               
               resultSet.close();
               statement.close();
               System.out.println(customer.getCity());
               return customer;
                
			} catch (SQLException e) {
               e.printStackTrace();
           }
           return null;
	}



	@Override
	public Customer updateCustomer(Customer cust) {
		  int id = cust.getCustomerId();
			try {
	              // Statement statement = getConnection().createStatement();
	              
				String queryString = String.format("UPDATE `mcda551002`.customer SET firstName='%s', lastname ='%s',"
	               		+ "streetnumber='%s',streetname='%s',city='%s',province='%s',country='%s',"
	               		+ "phone='%s', email='%s', postalcode='%s' WHERE id ="+id, cust.getFirstName(), 
	               		cust.getLastName(), cust.getStreetNumber(), cust.getStreetName(),cust.getCity(), cust.getProvince(),
	               		cust.getCountry(), cust.getPhoneNumber(), cust.getEmail(),cust.getPostalCode());
	               
				System.out.println(queryString);
				PreparedStatement statement = Connect_DB.getConnection().prepareStatement(queryString);
	            //statement.executeQuery();
				statement.executeUpdate();   
				statement.close();
	                
				} catch (SQLException e) {
	               e.printStackTrace();
	           }
	           return null;
		
	}

	@Override
	public void deleteCustomer(int id) {
		/*
		PreparedStatement preparedStatement = getConnection().prepareStatement(queryString);
        preparedStatement.executeUpdate();
		*/
	}

	
}
