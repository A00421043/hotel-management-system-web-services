package org.group.jdbcDAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import org.group.dao.CreditCardDAO;
import org.group.dao.CustomerDAO;
import org.group.dao.ReservationDAO;
import org.group.global.Connect_DB;
import org.group.models.Reservation;

public class JDBReservationDAO implements ReservationDAO{
	
	@Override
	public Reservation addReservation(Reservation reserve) {
        try {

        	String queryString = String.format("INSERT INTO `mcda551002`.`Reservation`\r\n" + 
            		//"(`ReservationId`,\r\n" + 
            		"(`CheckInDate`,\r\n" + 
            		"`CheckOutDate`,\r\n" + 
            		"`RoomNumber`,\r\n" + 
            		"`ReservationStatus`,\r\n" + 
            		"`CustomerId`,\r\n" + 
            		"`CreditCardId`,\r\n" + 
            		"`HotelId`,\r\n" + 
            		"`AgentId`,\r\n" + 
            		"`Guests`)\r\n" + 
            		"VALUES\r\n" + 
            		"(%s,%s,%s,%s,%d,%d,%d,%s,%d)","'"+ reserve.getCheckInDate()+"'","'"+reserve.getCheckOutDate()+"'",
            		"'"+reserve.getRoomNumber()+"'","'"+reserve.getReservationStatus()+"'",reserve.getCustomerId(),reserve.getCreditCardId(),reserve.getHotelId(),"'"+reserve.getAgentId()+"'",reserve.getGuests());
        	System.out.println(reserve.getCustomer().getCustomerId());
            PreparedStatement preparedStatement = Connect_DB.getConnection().prepareStatement(queryString);
            System.out.println(queryString);
            System.out.println(reserve.getCheckInDate());
            System.out.println(reserve.getCustomerId());
//            System.out.println(reserve.customer.getFirstName());
//            System.out.println(reserve.customer.getLastName());
//            System.out.println(reserve.customer.getEmail());
//            System.out.println(reserve.customer.getProvince());
//            System.out.println(reserve.customer.getCity());
//            System.out.println(reserve.customer.getPhoneNumber());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
         
    }

	@Override   
    public List<Reservation> getAllReservation() {
        List<Reservation> reserves = new LinkedList<Reservation>();
         try {
                Statement statement = Connect_DB.getConnection().createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT * FROM `mcda551002`.Reservation");
                CustomerDAO customerDAO;
                CreditCardDAO creditCardDAO;
                Reservation reserve = null;
                while(resultSet.next()){
                	reserve = new Reservation();
                	customerDAO=new JDBCustomerDAO();
                	creditCardDAO=new JDBCreditCardDAO();
                	reserve.setReservationId(Integer.parseInt(resultSet.getString("ReservationId")));
                	reserve.setCheckInDate(resultSet.getDate("CheckInDate").toString());
                	reserve.setCheckOutDate(resultSet.getDate("CheckOutDate").toString());
                	System.out.println(resultSet.getDate("CheckInDate"));
                	reserve.setRoomNumber(resultSet.getString("RoomNumber"));
                	reserve.setReservationStatus(resultSet.getString("ReservationStatus"));
                	reserve.setCustomerId(resultSet.getInt("CustomerId"));
                	reserve.setCreditCardId(resultSet.getInt("CreditCardId"));
                	reserve.setHotelId(resultSet.getInt("HotelId"));
                	reserve.setGuests(resultSet.getInt("Guests"));
                	reserve.setAgentId(resultSet.getString("AgentId"));
                	reserve.setCreditCard(creditCardDAO.getCreditCard(resultSet.getInt("CreditCardId")));
                	reserve.setCustomer(customerDAO.getCustomer(resultSet.getInt("CustomerId")));
                	reserves.add(reserve);
                }
                resultSet.close();
                statement.close();
                 
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return reserves;
    }
	
	@Override
	public Reservation getReservation(int id) {
		List<Reservation> reserves = new LinkedList<Reservation>();
		try {
               Statement statement = Connect_DB.getConnection().createStatement();
               ResultSet resultSet = statement.executeQuery("SELECT * FROM `mcda551002`.Reservation WHERE ReservationId="+id);
                
               Reservation reserve = null;
               while(resultSet.next()){
            	   reserve = new Reservation();
            	   reserve.setReservationId(Integer.parseInt(resultSet.getString("ReservationId")));
            	   reserve.setCheckInDate(resultSet.getDate("CheckInDate").toString());
            	   reserve.setCheckOutDate(resultSet.getDate("CheckOutDate").toString());
            	   reserve.setReservationStatus(resultSet.getString("ReservationStatus"));
            	   reserve.setRoomNumber(resultSet.getString("RoomNumber"));
            	   reserve.setCustomerId(resultSet.getInt("CustomerId"));
            	   reserve.setCreditCardId(resultSet.getInt("CreditCardId"));
            	   reserve.setHotelId(resultSet.getInt("Hotelid"));
            	   reserve.setAgentId(resultSet.getString("AgentId"));
            	   reserve.setGuests(resultSet.getInt("Guests"));
            	   
            	   reserves.add(reserve);  
               }
               
               resultSet.close();
               return reserve;
                
			} catch (SQLException e) {
               e.printStackTrace();
           }
           return null;
	}
	
	@Override
	public Reservation updateReservation(Reservation reserve) {
		  int id = reserve.getReservationId();
			try {
	              // Statement statement = getConnection().createStatement();
	              
				String queryString = String.format("UPDATE `mcda551002`.Reservation SET CheckInDate='%s', CheckOutDate ='%s',"
	               		+ "RoomNumber='%s',ReservationStatus='%s WHERE ReservationId ="+id, reserve.getCheckInDate(), 
	               		reserve.getCheckOutDate(), reserve.getRoomNumber(), reserve.getReservationStatus()+"'");
	               
				System.out.println(queryString);
				PreparedStatement statement = Connect_DB.getConnection().prepareStatement(queryString);
				statement.executeUpdate();   
				statement.close();
				//int customerId=reserve.getCustomer().getCustomerId();
				//System.out.println(customerId);
				CustomerDAO customerDAO=new JDBCustomerDAO();
				customerDAO.updateCustomer(reserve.getCustomer());
				CreditCardDAO creditCardDAO=new JDBCreditCardDAO();
				creditCardDAO.updateCreditCard(reserve.getCreditCard());
				} catch (SQLException e) {
	               e.printStackTrace();
	           }
	           return null;
		
	}
	
	@Override
	public void deleteReservation(int ReservationId) {
		
		try {
              // Statement statement = getConnection().createStatement();
              
			String queryString = "UPDATE Reservation SET ReservationStatus='Cancel' WHERE ReservationId ="+ReservationId;
               
			System.out.println(queryString);
			PreparedStatement statement = Connect_DB.getConnection().prepareStatement(queryString);
			statement.executeUpdate();   
			statement.close();
			
			} catch (SQLException e) {
               e.printStackTrace();
           }
          
	}

}
